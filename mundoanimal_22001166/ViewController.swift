//
//  ViewController.swift
//  mundoanimal_22001166
//
//  Created by COTEMIG on 17/11/22.
//

import UIKit
import Kingfisher
import Alamofire

struct MundoAnimal: Decodable {
    let name: String
    let animal: String
    let latin: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var lista:[MundoAnimal] = []
    var userDefaults = UserDefaults.standard
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lista.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let itens = TableView.dequeueReusableCell(withIdentifier: "MinhaCelula",for: indexPath) as! MinhaCelula
        let retorno = lista[indexPath.row]
        
        itens.animalnome.text = retorno.name
        itens.animallatin.text = retorno.latin
        itens.animalimg.kf.setImage(with: URL(string: retorno.animal))
        
        return itens
    }
    func Novo() {
            AF.request("https://zoo-animal-api.herokuapp.com/animals/rand").responseDecodable(of: [MundoAnimal].self) { response in
                if let api = response.value {
                    self.lista = api
                    print(api)
                }
                self.TableView.reloadData()
            }
        }
    @IBOutlet weak var TableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        TableView.dataSource = self
        Novo()
    }
}
