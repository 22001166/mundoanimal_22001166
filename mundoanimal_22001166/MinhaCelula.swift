//
//  MinhaCelula.swift
//  mundoanimal_22001166
//
//  Created by COTEMIG on 17/11/22.
//

import UIKit

class MinhaCelula: UITableViewCell {
    
    @IBOutlet weak var animalimg: UIImageView!
    @IBOutlet weak var animallatin: UILabel!
    @IBOutlet weak var animalnome: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
